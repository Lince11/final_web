import * as React from 'react';

import { Login } from './Login';
import { Home } from './Home';

export class Root extends React.Component<any,any>{
    state = {
        pagina: 'login', 
        usuario: null,
    }

    componentWillMount(){
        this.validarLogeado();
    }

    cambioPagina(nuevaPagina){
        this.setState({
            pagina: nuevaPagina
        });
    }

    setUsuario(usuario){
        this.setState({
            usuario: usuario
        })
    }

    validarLogeado(){
        var usuario = JSON.parse(localStorage.getItem('usuario'));
        if(usuario) {
            this.setState({
                usuario: usuario,
                pagina: 'home'
            });
        }
    }

    render(){
        switch(this.state.pagina){
            case 'login': 
                return <Login setUsuario={u => this.setUsuario(u)} 
                            cambioPagina={p => this.cambioPagina(p)} />;
            case 'home': 
                return <Home cambioPagina={p => this.cambioPagina(p)}
                            usuario={this.state.usuario} />;
        }
    }
}


