import * as React from 'react';
import Dropzone from 'react-dropzone';

export class Home extends React.Component<any, any>{

    state = {
        imagen: null
    }

    subirImagen(imagen) {
        var params = new FormData();
        params.append('img', imagen);
        params.append('user', this.props.usuario.user);
        params.append('description', 'test');

        fetch(`http://localhost:8080/api/upload/${this.props.usuario.user}`, {
            method: 'post',
            body: params
        })
            .then(e => e.json())
            .then(res => {
                console.log(res);
            });
    }

    cargarImagen(accepted, rejected) {
        console.log(accepted)
        this.setState({
            imagen: accepted[0]
        })
    }

    render() {
        return <div>
            <h1>HOME {this.props.usuario.user}</h1>

            {!this.state.imagen && <Dropzone onDrop={(accepted, rejected) => this.cargarImagen(accepted, rejected)} />}
            {this.state.imagen && <div>
                <img style={{ width: 300, display: 'block' }} src={this.state.imagen.preview} />
                <p>{this.state.imagen.name}</p>
                <button className="btn btn-danger"
                    onClick={() => this.setState({ imagen: null })}>cancelar</button>
                <button className="btn btn-primary"
                    onClick={() => this.subirImagen(this.state.imagen)}>subir</button>
            </div>
        }
        </div>
    }
}
