import * as React from 'react';

export class Login extends React.Component<any,any>{

    state = {
        mensaje: ''
    }

    hacerLogin(evento) {
        evento.preventDefault();
        var form: any = evento.target;
        var params = new URLSearchParams();
        params.append('user', form.usuario.value);
        params.append('pass', form.contrasena.value);

        fetch('http://localhost:8080/api/login', {
            method: 'post',
            body: params
        })
            .then(e => e.json())
            .then(res => {
                this.setState({
                    mensaje: res.mensaje
                });
                console.log(res);

                if(res.mensaje == 'logged') {
                    localStorage.setItem('usuario', JSON.stringify(res.user));

                    this.props.setUsuario(res.user);
                    this.props.cambioPagina('home');
                }
            });
    }
    
    render(){
        return <div className="contenedor">
				<div className="landing">
					<div className="logotipo">
						<img src="imagenes/scribble.svg"/>
							<p>¡Ease your writing!</p>
					</div>
				</div>

					<div className="logeo">
						<div className="tit-seccion">
							<p>Iniciar Sesión</p>
						</div>
						<form onSubmit={e => this.hacerLogin(e)} className="formLog">
							<div className="seccion">
							<input className="form" name="usuario" placeholder="    Correo electrónico"/>
							<input type="password" className="form" name="contrasena" placeholder="    Contraseña"/>
							</div>
							<div className="botIniciar">
							<button type="submit" className="inic">Iniciar</button>
							</div>
							<div className="recomendacion">
							<p> ¿Olvidaste tu contraseña? </p>
							</div>
						</form>
							<div className="botRegistro">
							<a href="registro.html"><button id="toRegistro">¡Registrate!</button></a>
							</div>
					</div>
                    {this.state.mensaje}
			</div>;
		
    }
}
