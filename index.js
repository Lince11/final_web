const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const api = require('./api.js');
const fileUpload = require('express-fileupload');


var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: "true" }));
app.use(express.static(path.join(__dirname, "final_web_hurtadoLince")));
app.use("/api", api);
app.get("/", (req, res) => {
    res.sendFile(path.join(__dirname, "./index.html"));
});
app.listen(3000); //lo pongo en 8080 y no renderiza