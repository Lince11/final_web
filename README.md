## Instalación:
* Click derecho -> Git Bash Here
* git clone https://vagyriag@bitbucket.org/vagyriag/react_seed.git
* cd react_seed
* npm install

## Uso:
* npm run dev
* Abrir [http://localhost:8080](http://localhost:8080)

## Exportar:
* npm run prod